#ifndef NANOCOM_TYPES_H
#define NANOCOM_TYPES_H

#if defined(NCOM_STATIC)
#  define NCOM_APICALL
#else
#  if defined(_WIN32)
#    define NCOM_APICALL	__declspec(dllimport)
#  elif defined(__GNUC__) && __GNUC__ >= 4
#    define NCOM_APICALL	__attribute__ ((visibility ("default")))
#  else
#    define NCOM_APICALL
#  endif
#endif

#if defined(_WIN32)

/* Windows is not C99-conformant, so provide the standard types */
typedef __int8			int8_t;
typedef unsigned __int8		uint8_t;
typedef __int16			int16_t;
typedef unsigned __int16	uint16_t;
typedef __int32			int32_t;
typedef unsigned __int32	uint32_t;
typedef __int64			int64_t;
typedef unsigned __int64	uint64_t;

#if defined(_WIN64)

typedef long long		ssize_t;
/* TODO: does windows have its own size_t? */
typedef unsigned long long	size_t;
typedef long long		intptr_t;
typedef unsigned long long	uintptr_t;

#define NCOM_APIENTRY		__stdcall

#else

typedef long			ssize_t;
typedef unsigned long		size_t;
typedef long			intptr_t;
typedef unsigned long		uintptr_t;

#endif /* defined(_WIN64) */

#else

#include <stddef.h>
#include <stdint.h>

#define NCOM_APIENTRY

#endif /* defined(_WIN32) */

typedef int32_t		NCOMbool;
enum {
	NCOM_FALSE	= 0,
	NCOM_TRUE	= 1,
};

#endif /* NANOCOM_TYPES_H */
