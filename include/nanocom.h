#ifndef NANOCOM_H
#define NANOCOM_H

#include <string.h>

#include <ncomplatform.h>

#ifdef __cplusplus
extern "C" {
#endif

/* error handling, compatible with HRESULT */

typedef int32_t			NCOMResult;

#define ncomFailed(hr)		(((NCOMResult)(hr)) < 0)
#define ncomSucceeded(hr)	(((NCOMResult)(hr)) >= 0)

enum {
	NCOM_S_OK		= 0x00000000,
	NCOM_S_FALSE		= 0x00000001,
	NCOM_E_PENDING		= 0x8000000A,
	NCOM_E_BOUNDS		= 0x8000000B,
	NCOM_E_NOTIMPL		= 0x80004001,
	NCOM_E_NOINTERFACE	= 0x80004002,
	NCOM_E_POINTER		= 0x80004003,
	NCOM_E_ABORT		= 0x80004004,
	NCOM_E_FAIL		= 0x80004005,
	NCOM_E_UNEXPECTED	= 0x8000FFFF,
	NCOM_E_ACCESSDENIED	= 0x80070005,
	NCOM_E_HANDLE		= 0x80070006,
	NCOM_E_OUTOFMEMORY	= 0x8007000E,
	NCOM_E_INVALIDARG	= 0x80070057,
};

/* GUID */

typedef struct {
	uint32_t	data1;
	uint16_t	data2;
	uint16_t	data3;
	uint8_t		data4[8];
} 	NCOMGuid;
typedef NCOMGuid	NCOMIid;

#define NCOM_DECLARE_GUID(name)	extern const NCOMGuid name
#define NCOM_DEFINE_GUID(name, d1, d2, d3, d41, d42, d43, d44, d45, d46, d47, d48)	\
	const NCOMGuid name = {								\
		d1, d2, d3, { d41, d42, d43, d44, d45, d46, d47, d48 }}

#define ncomIsEqualGUID(a, b)	(!memcmp((a), (b), sizeof(NCOMGuid))	\
				    ? NCOM_TRUE : NCOM_FALSE)
#define ncomIsEqualIID(a, b)	ncomIsEqualGUID((a), (b))

/* IUnknown interface */

/* {00000000-0000-0000-C000-000000000046} */
#ifdef NCOM_IMPLEMENTATION
NCOM_DEFINE_GUID(NCOMIidIUnknown, 0, 0, 0, 0xC0, 0, 0, 0, 0, 0, 0, 0x46);
#else
NCOM_DECLARE_GUID(NCOMIidIUnknown);
#endif

typedef struct NCOMIUnknown	NCOMIUnknown;

typedef NCOMResult	(NCOM_APIENTRY *NCOMqueryInterfaceFn)(NCOMIUnknown*,
			    const NCOMIid*, void**);
typedef uint32_t	(NCOM_APIENTRY *NCOMaddRefFn)(NCOMIUnknown*);
typedef uint32_t	(NCOM_APIENTRY *NCOMreleaseFn)(NCOMIUnknown*);

typedef struct NCOMIUnknownVTable {
	NCOMqueryInterfaceFn	queryInterface;
	NCOMaddRefFn		addRef;
	NCOMreleaseFn		release;
} NCOMIUnknownVTable;

struct NCOMIUnknown {
	const NCOMIUnknownVTable	*vtable;
};

#define ncomIUnknownQueryInterface(this, iid, p)	\
    ((this)->vtable->queryInterface((this), (iid), (p)))
#define ncomIUnknownAddRef(this)	((this)->vtable->addRef(this))
#define ncomIUnknownRelease(this)	((this)->vtable->release(this))

#ifdef __cplusplus
}
#endif

#endif /* NANOCOM_H */
