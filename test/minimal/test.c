#define NCOM_IMPLEMENTATION
#include <nanocom.h>

#include <stdio.h>
#include <stdlib.h>

/* union build */
#include "../INamed/INamed.c"

int main() {
	INamed		*p;
	const char	*str;
	uint32_t	len;
	NCOMResult	hr;

	hr = createNamed(&IID_INamed, (void**)&p);
	if (ncomSucceeded(hr)) {
		hr = INamed_getName(p, &str, &len);
		if (ncomSucceeded(hr)) {
			printf("%s\n", str);
		} else {
			fprintf(stderr, "failed to get name: 0x%08x\n", hr);
		}
		INamed_release(p);
	} else {
		fprintf(stderr, "failed to create object: 0x%08x\n", hr);
	}
	exit(ncomFailed(hr));
}
