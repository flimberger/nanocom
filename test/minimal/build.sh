#!/bin/sh
: ${CC:=cc}
CFLAGS="-Wall -Wextra -std=c89 -I../../include -DNCOM_STATIC=1"
$CC $CFLAGS -o test test.c
