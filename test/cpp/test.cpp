#include <nanocom.h>

#include <stdio.h>
#include <stdlib.h>

extern "C" NCOMIid IID_INamed;

extern "C" NCOMResult createNamed(NCOMIid const*, void**);

struct INamed {
    virtual NCOMResult queryInterface(NCOMIid const*, void**) = 0;
    virtual uint32_t addRef(void) = 0;
    virtual uint32_t release(void) = 0;

    virtual NCOMResult getName(const char**, uint32_t*);
};

int main() {
    INamed *p;
    const char *str;
    uint32_t len;
    NCOMResult hr;

    hr = createNamed(&IID_INamed, reinterpret_cast<void**>(&p));
    if (ncomSucceeded(hr)) {
        hr = p->getName(&str, &len);
        if (ncomSucceeded(hr)) {
            printf("%s\n", str);
        } else {
            fprintf(stderr, "failed to get name: 0x%08x\n", hr);
        }
        p->release();
    } else {
        fprintf(stderr, "failed to create object: 0x%08x\n", hr);
    }
    exit(ncomFailed(hr));
}
