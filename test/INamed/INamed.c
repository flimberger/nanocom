#include "INamed.h"

#include <stdlib.h>
#include <string.h>

struct INamedImpl {
	const INamedVtbl	*vtable;
	const char		*name;
	uint32_t		 len;
	uint32_t		 refcount;
};

/* {78D90FFC-4B13-42FF-AEC9-0BFC85937F38} */
NCOM_DEFINE_GUID(IID_INamed,
	0x78d90ffc, 0x4b13, 0x42ff, 0xae, 0xc9, 0xb, 0xfc, 0x85, 0x93, 0x7f, 0x38);

static
NCOMResult NCOM_APIENTRY
queryInterface(INamed *this, const NCOMGuid *iid, void **pout)
{
	if (!ncomIsEqualIID(iid, &IID_INamed) &&
	    !ncomIsEqualIID(iid, &NCOMIidIUnknown)) {
		*pout = NULL;
		return NCOM_E_NOINTERFACE;
	}
	*pout = this;
	INamed_addRef(this);
	return NCOM_S_OK;
}

static
uint32_t NCOM_APIENTRY
addRef(INamed* this)
{
	struct INamedImpl	*p;

	p = (struct INamedImpl *)this;
	p->refcount++;
	return p->refcount;
}

static
uint32_t NCOM_APIENTRY
release(INamed* this)
{
	struct INamedImpl	*p;

	p = (struct INamedImpl *)this;
	p->refcount--;
	if (p->refcount == 0) {
		free(p);
		return 0;
	}
	return p->refcount;
}

static
NCOMResult NCOM_APIENTRY
getName(INamed* this, const char **pname, uint32_t *plen)
{
	struct INamedImpl	*p;

	p = (struct INamedImpl *)this;
	*pname = p->name;
	*plen = p->len;
	return NCOM_S_OK;
}

static const
INamedVtbl inamedVTable = {
	/* .QueryInterface = */	queryInterface,
	/* .AddRef = */		addRef,
	/* .Release = */	release,
	/* .GetName = */	getName,
};

NCOMResult
createNamed(const NCOMIid *iid, void **p)
{
	struct INamedImpl	*i;
	NCOMResult		hr;

	i = malloc(sizeof(*i));
	if (i) {
		i->vtable = &inamedVTable;
		i->refcount = 1;
		i->name = "test";
		i->len = (uint32_t)strlen(i->name);
		hr = inamedVTable.queryInterface((INamed *)i, iid, p);
		inamedVTable.release((INamed *)i);
	} else {
		hr = NCOM_E_OUTOFMEMORY;
	}
	return hr;
}
