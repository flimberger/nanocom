#ifndef NANOCOM_EXAMPLE_INAMED_H
#define NANOCOM_EXAMPLE_INAMED_H

#include <nanocom.h>

typedef struct INamed	INamed;

typedef struct INamedVtbl {
	/* IUnknown */
	NCOMResult	(NCOM_APIENTRY *queryInterface)(INamed*,
			    const NCOMIid*, void**);
	uint32_t	(NCOM_APIENTRY *addRef)(INamed*);
	uint32_t	(NCOM_APIENTRY *release)(INamed*);

	/* INamed */
	NCOMResult	(NCOM_APIENTRY *getName)(INamed*, const char**,
			    uint32_t*);
} INamedVtbl;

struct INamed {
	const INamedVtbl	*vtable;
};

#if !defined(INAMED_STATIC)
#  if defined(_WIN32)
#    if defined(INAMED_EXPORTS)
#      define INAMED_IMPL	__declspec(dllexport)
#    else
#      define INAMED_IMPL	__declspec(dllimport)
#    endif
#  elif defined(__GNUC__) && __GNUC__ >= 4
#    define INAMED_IMPL	__attribute__ ((visibility ("default")))
#  endif
#else
#  define INAMED_IMPL
#endif

INAMED_IMPL NCOM_DECLARE_GUID(IID_INamed);

INAMED_IMPL NCOMResult	createNamed(const NCOMIid*, void**);

#define INamed_queryInterface(this, iid, p)	\
    ((this)->vtable->queryInterface(this, iid, p))
#define INamed_addRef(this)	((this)->vtable->addRef(this))
#define INamed_release(this)	((this)->vtable->release(this))

#define INamed_getName(this, p, len)	\
    ((this)->vtable->getName((this), (p), (len)))

#endif /* NANOCOM_EXAMPLE_INAMED_H */
