rem build a dll
cl /nologo /W4 /LD /TC /I..\..\include /DNCOM_STATIC /DINAMED_EXPORTS named.c
if %errorlevel% neq 0 exit /b %errorlevel%
rem build the test executable which imports the dll
cl /nologo /W4 /TC test.c /link named.lib
