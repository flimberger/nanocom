#include <combaseapi.h>

#include <stdio.h>

#undef INTERFACE
#define INTERFACE  INamed
DECLARE_INTERFACE_ (INTERFACE, IUnknown)
{
    STDMETHOD  (QueryInterface) (THIS_ REFIID, void **) PURE;
    STDMETHOD_ (ULONG, AddRef)  (THIS) PURE;
    STDMETHOD_ (ULONG, Release) (THIS) PURE;
    STDMETHOD  (GetName)      (THIS_ LPCSTR*, ULONG*) PURE;
};

#define INamed_GetName(This, pName, nLen)    \
    ((This)->lpVtbl->GetName((This), (pName), (nLen)))
#define INamed_Release(This)    ((This)->lpVtbl->Release((This)))

__declspec(dllimport) extern IID  IID_INamed;

__declspec(dllimport) HRESULT createNamed(REFIID, void**);

int main()
{
    INamed  *pObj;
    LPCSTR  sName;
    ULONG   nLen;
    HRESULT hr;

    hr = createNamed(&IID_INamed, (void**)&pObj);
    if (SUCCEEDED(hr))
    {
        hr = INamed_GetName(pObj, &sName, &nLen);
        if (SUCCEEDED(hr))
        {
            printf("%s\n", sName);
        }
        else
        {
            printf("failed to get name: 0x%08x\n", hr);
        }
        INamed_Release(pObj);
    }
    else
    {
        printf("failed to create object: 0x%08x\n", hr);
    }
}
